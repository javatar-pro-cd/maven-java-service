package pro.javatar.pipeline.simple.service.example;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@EnableDiscoveryClient
@SpringBootApplication
public class SimpleJavaServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(SimpleJavaServiceApplication.class, args);
	}

}
